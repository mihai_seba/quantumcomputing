#include "stdafx.h"
#include "QCpp.h"
#include "Oracle.h"


Oracle::Oracle()
{
	gGateFactory = GateFactory();
	gGateX = gGateFactory.CreateGate(EXGate);
	gGateCNot = gGateFactory.CreateGate(ECnotGate);
}


Oracle::~Oracle()
{
}


void Oracle::ApplyOracle(Qubit *qubit,size_t registerSize)
{
	//Eigen::MatrixXcd identity = Eigen::MatrixXcd::Identity(gMatrixSize, gMatrixSize);
	//just set -1 to |00> or |01> or whatever |xx> solution
	//gGateCNot->Apply(qubit, 10, { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, registerSize);




	std::vector<size_t>affectedPosition;
	for (size_t i = 0; i < registerSize-1; i++) {
		//mark position for applying X-Gate
		affectedPosition.push_back(i);
		
	}
	//apply oracle 
	size_t target = affectedPosition.back() + 1;
	
	gGateCNot->Apply(qubit, target, affectedPosition, registerSize);
	

}