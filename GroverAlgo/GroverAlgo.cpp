// GroverAlgo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <fstream>
#include <chrono>
#include <ctime>


#include "Grover.h"
//A quantum oracle O which performs the operation O|x> = (-1)^f(x) |x>, where
//f(x) = 0 for all 0  x < 2^n except x0, for which f(x0) = 1.

int main(int argc, char* argv[])
{
//	auto t_start = std::chrono::high_resolution_clock::now();

	std::shared_ptr<ProfilerManager>profiler=std::make_shared<ProfilerManager>();


		
	profiler->RunProfiler(E_ProfilingOption::OptionCpuTime);
	Grover g = Grover();
	g.Init();

	g.Run();
	g.Measure();


	CpuTimeValues cpuTimeValues = profiler->GetCpuTimeProfilerResults();
				
	std::cout << cpuTimeValues.GetCpuTimeUs() << " us";

	size_t position=0;
	for (size_t i =g.GetResults().GetQubitStates().size()-1 ; i >=0 ; i--){
		if(g.GetResults().GetQubitStates()(i).real()==1.0){
			position=i;
			break;		
		}
	}
	std::cout<<"Position: "<<position<<'\n';
	std::cout<<"No of qubits: "<<Grover::NO_OF_INPUTS;
	std::cout << '\n';

/*
	auto t_end = std::chrono::high_resolution_clock::now();
	std::cout << std::chrono::duration<double, std::milli>(t_end - t_start).count()
		<< " ms\n";
*/
	return 0;
}

