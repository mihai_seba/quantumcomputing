#include "stdafx.h"
#include <cassert>
#include "Oracle.h"
#include "Grover.h"


#ifdef LARGE_NUMBER_OF_INPUT_QUBITS
const double Grover::NO_OF_INPUTS = 12;
#else
const double Grover::NO_OF_INPUTS = 3;
#endif
Grover::Grover() :QAlgorithms()
{



}


Grover::~Grover()
{

}



void Grover::Init()
{

	gGateH = gGateFactory.CreateGate(EHadamardGate);
	gGateX = gGateFactory.CreateGate(EXGate);
	gGateCNot = gGateFactory.CreateGate(ECnotGate);
	gGateCPhaseShift = gGateFactory.CreateGate(ECPhaseShift);


	gQregister = QRegister(NO_OF_INPUTS + 1);
	gRegOps = &QRegisterOperations::GetInstance();
	gRegOps->FillWithZero(&gQregister);

	gQregister[NO_OF_INPUTS] = QubitOne();
	gResultQubit = gQregister();
	//initialize oracle
	gOracle = std::make_shared<Oracle>();
	//set oracle object
	SetOracleObj(gOracle);
	SetOracleMethod(&Oracle::ApplyOracle, (NO_OF_INPUTS + 1));
}
void Grover::Run()
{

	size_t no = (size_t)std::sqrt(std::pow(2, NO_OF_INPUTS));
	std::vector<size_t>affectedPositions;


	for (size_t index = 0; index < NO_OF_INPUTS; index++) {

		affectedPositions.push_back(index);

	}

	
	
	//apply first stage -> hadamards
	affectedPositions.push_back(affectedPositions.back() + 1);
	gGateH->Apply(&gResultQubit, affectedPositions, (NO_OF_INPUTS + 1));


	//apply oracle
	ApplyOracle();
	affectedPositions.pop_back();


	for (size_t i = 0; i < no + 1; i++){
		//apply hadamards
		gGateH->Apply(&gResultQubit, affectedPositions, (NO_OF_INPUTS + 1));

		//apply X gates
		gGateX->Apply(&gResultQubit, affectedPositions, (NO_OF_INPUTS + 1));

		//apply Phase shift
		affectedPositions.pop_back();
		size_t target = affectedPositions.back() + 1;
		gGateCPhaseShift->Apply(&gResultQubit, target, affectedPositions, (NO_OF_INPUTS + 1));
		affectedPositions.push_back(target);

		//apply X gates
		gGateX->Apply(&gResultQubit, affectedPositions, (NO_OF_INPUTS + 1));

		affectedPositions.push_back(affectedPositions.back() + 1);
		gGateH->Apply(&gResultQubit, affectedPositions, (NO_OF_INPUTS + 1));
		affectedPositions.pop_back();

	}
	



	assert(gResultQubit.IsValid() == true);

}


void Grover::Measure()
{


	MeasurementPerformer measure = MeasurementPerformer(10000);
	measure.Configure(gResultQubit);
	gResultQubit = measure.Measure();


}


