#pragma once
class Oracle
{
public:
	Oracle();
	virtual ~Oracle();
	
	void ApplyOracle(Qubit *qubit, size_t registerSize);
private:
	GateFactory gGateFactory;
	std::shared_ptr<IGate> gGateX;
	std::shared_ptr<IGate> gGateCNot;
};

