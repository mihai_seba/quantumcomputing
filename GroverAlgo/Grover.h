#pragma once
class Grover :public QAlgorithms
{
public:
	Grover();
	~Grover();
	void Init();
	void Run();
	void Measure();
	static const double NO_OF_INPUTS;
private:
	
	
	std::shared_ptr<IGate> gGateH;
	std::shared_ptr<IGate> gGateX;
	std::shared_ptr<IGate> gGateCNot;
	std::shared_ptr<IGate> gGateCPhaseShift;
	std::shared_ptr<Oracle> gOracle;
	
	QRegisterOperations *gRegOps;

};

