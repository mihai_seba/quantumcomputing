import Data.Complex
import Data.List(transpose)

type Vector a=[a]
type Matrix a=[Vector a]

vectorToComplex::Integral a=>Vector a->Vector(Complex Double)
vectorToComplex=map(\i->fromIntegral i:+0.0)

matrixToComplex::Integral a=>Matrix a->Matrix(Complex Double)
matrixToComplex=map vectorToComplex

--Define Operators
(<*>) :: Num a => a -> Matrix a -> Matrix a
c <*> m = map (map (c *)) m

--Tensor product of 2 matrices
(<+>)::Num a=>Matrix a->Matrix a->Matrix a
m1 <+> m2=concatMap collateRows $ groups n [c<*>m2|c<-concat m1]
   where
   n=length $ head m1
   groups::Int->[a]->[[a]]
   groups i s 
          | null s=[]
	  | otherwise=let (h,t)=splitAt i s in h:groups i t
   collateRows::[Matrix a]->Matrix a
   collateRows=map concat . transpose
  



-- Function that will combine qubits in one quantum register
entangle :: Num a=>Vector a->Vector a->Vector a
entangle q1 q2=[qs1*qs2| qs1<-q1, qs2<-q2]

--Apply function will apply the defined gate to a define qubit
apply::Num a=>Matrix a->Vector a->Vector a
apply m v=map(sum . zipWith(*) v) m


(|>)::Num a=>Vector a->Matrix a->Vector a
(|>)= flip apply

-- Define Qubits
-- qubit |0>
qubitZero::Vector (Complex Double)
qubitZero=vectorToComplex[1,0]

--qubit |1>
qubitOne::Vector (Complex Double)
qubitOne=vectorToComplex[0,1]


--XNOT gate
gateX::Matrix(Complex Double)
gateX=matrixToComplex[[0,1],[1,0]]

--Hadamard gate
gateH::Matrix(Complex Double)
gateH=((1/sqrt 2):+0.0) <*> matrixToComplex[[1,1],[1,-1]]

--Z Gate
gateZ::Matrix(Complex Double)
gateZ=matrixToComplex[[1,0],[0,-1]]

gateY::Matrix(Complex Double)
gateY=matrixToComplex[[0,0:+(-1)],[0:+1,0]]                     

--gateR::Floating t=>t->Matrix(Complex Double)
--gateR t=matrixToComplex [[cos t,-sin t],[sin t, cos t]]                

        
--Deutschs algorithm implementation
deutsch' :: Matrix (Complex Double) -> IO ()
deutsch' f = do let (result:_) = measure circuit
                case result of
                  '0' -> putStrLn "Function f is constant."
                  '1' -> putStrLn "Function f is balanced."
                  _   -> return ()
  where
    gateH2  = gateH <+> gateH
    circuit = entangle qubitZero (qubitZero |> gateX) |> gateH2
                                                      |> f
                                                      |> gateH2
    measure q = let result = map (\c -> round (realPart (c * conjugate c))) q
                in  case result of
                      [0, 1, 0, 0] -> "01"
                      [0, 0, 0, 1] -> "11"
                      _            -> "??"


        


-- | Function to test classic implementation of Deutsch’s algorithm.
testDeutsch :: IO ()
testDeutsch = mapM_ deutsch' [f1, f2, f3, f4]
f1 :: Matrix (Complex Double)
f1 = matrixToComplex [[1, 0, 0, 0],
                       [0, 1, 0, 0],
                       [0, 0, 1, 0],
                       [0, 0, 0, 1]]
-- | Unitary transformation to represent quantum oracle of a function
--   \f x = 1\.
f2 :: Matrix (Complex Double)
f2 = matrixToComplex [[0, 1, 0, 0],
                       [1, 0, 0, 0],
                       [0, 0, 0, 1],
                       [0, 0, 1, 0]]
-- | Unitary transformation to represent quantum oracle of a function
--   \f x = x\.
f3 :: Matrix (Complex Double)
f3 = matrixToComplex [[1, 0, 0, 0],
                       [0, 1, 0, 0],
                       [0, 0, 0, 1],
                       [0, 0, 1, 0]]
-- | Unitary transformation to represent quantum oracle of a function
--   \f x = not x\.
f4 :: Matrix (Complex Double)
f4 = matrixToComplex [[0, 1, 0, 0],
                       [1, 0, 0, 0],
                       [0, 0, 1, 0],
                       [0, 0, 0, 1]]
