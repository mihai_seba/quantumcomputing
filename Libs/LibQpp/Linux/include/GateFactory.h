#pragma once
#ifdef WINDOWS
	#ifdef LIBQPP_EXPORTS
		#define LIBQPP_API __declspec(dllexport)
	#else
		#define LIBQPP_API __declspec(dllimport)
	#endif
#elif defined(LINUX)
	#define LIBQPP_API 
#endif // _WIN32

enum LIBQPP_API EGateType :uint8_t
{
	E_NO_GATE_SELECTED = 0x00,
	EHadamardGate = 0x01,
	ECnotGate = 0x02,
	EXGate = 0x03,
	EZGate = 0x04,
	EYGate = 0x05,
	ECPhaseShift = 0x06
};

class LIBQPP_API AGatesFactory
{
public:
	inline AGatesFactory(){}
	inline virtual ~AGatesFactory(){}
	virtual std::shared_ptr<IGate> CreateGate(enum EGateType gateType) = 0;
};
class LIBQPP_API GateFactory:public AGatesFactory
{
public:
	GateFactory();
	virtual ~GateFactory();
	std::shared_ptr<IGate> CreateGate(enum EGateType gateType);
	
};

