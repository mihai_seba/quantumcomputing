#include <memory>
#include <iostream>
#include <mutex>
#include <complex>
#include <algorithm>
#include <vector>
#include <exception>
#include <cmath>
#include <bitset>
#include <cstdlib>
#include <random>
#include <iterator>
#include <mkl.h>
#ifndef		EIGEN_USE_MKL_ALL
	#define		EIGEN_USE_MKL_ALL
#endif
#include <Eigen/Dense>

#include "Qubit.h"
#include "OperationExceptions.h"
#include "QRegister.h"
#include "IGate.h"
#include "GateFactory.h"



#include "QRegisterOperations.h"
#include "QAlgorithms.h"
#include "MeasurementPerformer.h"



