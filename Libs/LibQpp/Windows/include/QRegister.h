#pragma once
#ifdef WINDOWS
	#ifdef LIBQPP_EXPORTS
		#define LIBQPP_API __declspec(dllexport)
	#else
		#define LIBQPP_API __declspec(dllimport)
	#endif
#elif defined(LINUX)
	#define LIBQPP_API 
#endif // _WIN32

class LIBQPP_API QRegister
{
public:
	QRegister();
	QRegister(const size_t registerSize);
	~QRegister();
	void push(const Qubit q) throw (std::exception);
	void reserve_space(const size_t registerSize);
	size_t size() const;
	Qubit pop();
	Qubit &operator[](const size_t idx);
	Qubit at(const size_t idx);
	bool operator==(const QRegister& lhs) const;
	bool operator!=(const QRegister& lhs) const;
	typedef std::vector<Qubit>::iterator iterator;
	typedef std::vector<Qubit>::const_iterator const_iterator;
	inline iterator begin() { return gData.begin(); }
	inline iterator end() { return gData.end(); }
	Qubit operator()(void);
	
private:
	size_t gRegisterSize;
	std::vector<Qubit> gData;
	
};

