#pragma once
#ifdef WINDOWS
#ifdef LIBQPP_EXPORTS
#define LIBQPP_API __declspec(dllexport)
#else
#define LIBQPP_API __declspec(dllimport)
#endif
#elif defined(LINUX)
#define LIBQPP_API 
#endif // _WIN32

class LIBQPP_API MeasurementPerformer
{
public:
	MeasurementPerformer();
	MeasurementPerformer(size_t numberOfTests);
	virtual ~MeasurementPerformer();
	void Configure(Qubit resultQubit);
	Qubit Measure();

private:
	void CollapseState(size_t position);
	size_t IntervalCount();
	Qubit gResultQubit;
	size_t gLength;
	size_t gNumberOfTests;
	static const double Q_TOLERANCE;
	static const size_t DEFAULT_NO_OF_TESTS;
};