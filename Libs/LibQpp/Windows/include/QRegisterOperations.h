#pragma once
#ifdef WINDOWS
#ifdef LIBQPP_EXPORTS
#define LIBQPP_API __declspec(dllexport)
#else
#define LIBQPP_API __declspec(dllimport)
#endif
#elif defined(LINUX)
#define LIBQPP_API 
#endif // _WIN32
class LIBQPP_API QRegisterOperations
{
public:
#pragma region Singleton specific methods
	static QRegisterOperations &GetInstance();
	inline virtual ~QRegisterOperations(){}
#pragma endregion
#pragma region Operations with QRegister
	void FillWithZero(QRegister *quantumRegister);
	void FillWithPattern(QRegister *quantumRegister, const std::string bitString) throw (std::exception);
	void CopyRegister(QRegister *target, QRegister *source, size_t startingPosition);
#pragma endregion
protected:

private:
#pragma region Singleton fields
	static std::unique_ptr<QRegisterOperations> instance_;
	static std::once_flag gOnceFlag;
	inline QRegisterOperations(){}
	QRegisterOperations(const QRegisterOperations& rs) = delete;
	QRegisterOperations& operator = (const QRegisterOperations& rs) = delete;
#pragma endregion
};

