#pragma once
#ifdef WINDOWS
	#ifdef LIBQPP_EXPORTS
		#define LIBQPP_API __declspec(dllexport)
	#else
		#define LIBQPP_API __declspec(dllimport)
	#endif
#elif defined(LINUX)
	#define LIBQPP_API 
#endif // _WIN32

class LIBQPP_API Qubit
{
public:
	inline Qubit(){}
	Qubit(std::complex<double> state0, std::complex<double> state1);
	Qubit(Eigen::VectorXcd qubitStates);
	virtual ~Qubit();
	bool IsValid();
	Eigen::VectorXcd GetQubitStates() const;
	bool operator==(const Qubit& lhs) const;
	bool operator!=(const Qubit& lhs) const;
	friend Qubit operator*(const Qubit& lhs, const Qubit& rhs)
	{
		return Qubit::Entangle(lhs.GetQubitStates(), rhs.GetQubitStates());
	}
	std::string to_string() const;
	bool IsQubitMeasured() const;
	void MeasureQubit(void);
protected:
	//qubit States
	Eigen::VectorXcd gQubitStateArray;
	bool gIsQubitMeasured;
	static const double Q_TOLERANCE;
private:
	static Qubit Entangle(const Eigen::VectorXcd q1, const Eigen::VectorXcd q2);
	friend std::ostream& operator<<(std::ostream& os, const Qubit& obj)
	{
		os << obj.to_string();
		return os;
	}
	

};


class LIBQPP_API QubitZero :public Qubit
{
public:
	QubitZero() :Qubit(std::complex<double>(1.0, 0.0), std::complex<double>(0.0, 0.0))
	{

	}
	virtual ~QubitZero()
	{

	}

};

class LIBQPP_API QubitOne :public Qubit
{
public:
	QubitOne() :Qubit(std::complex<double>(0.0, 0.0), std::complex<double>(1.0, 0.0))
	{

	}
	virtual ~QubitOne()
	{

	}
};

class LIBQPP_API QubitMinus :public Qubit
{
public:
	QubitMinus() :Qubit(
		std::complex<double>(1.0 / std::sqrt(2), 0.0),
		std::complex<double>(-1.0 / std::sqrt(2), 0.0))
	{

	}
	virtual ~QubitMinus()
	{

	}
};

class LIBQPP_API QubitPlus :public Qubit
{
public:
	QubitPlus() :Qubit(
		std::complex<double>(1.0 / std::sqrt(2), 0.0),
		std::complex<double>(1.0 / std::sqrt(2), 0.0))
	{

	}
	virtual ~QubitPlus()
	{

	}
};