#ifdef WINDOWS
#ifdef CPUTIMEPROFILERDLL_EXPORTS
#define CPUTIMEPROFILERDLL_API __declspec(dllexport)
#else
#define CPUTIMEPROFILERDLL_API __declspec(dllimport)
#endif
#elif defined(LINUX)
#define CPUTIMEPROFILERDLL_API 
#endif // _WIN32
#include "stdafx.h"
// This class is exported from the CpuTimeProfilerDll.dll
class CPUTIMEPROFILERDLL_API CpuTimeValues
{
public:
	CpuTimeValues();
	virtual ~CpuTimeValues();
	void FillValues(const uint64_t cpu_time_ns, const uint64_t user_time_ns, const uint64_t kernel_time_ns);
	uint64_t GetCpuTimeNs() const;
	uint64_t GetUserTimeNs() const;
	uint64_t GetKernelTimeNs() const;
	
	double GetCpuTimeUs() const;
	double GetUserTimeUs() const;
	double GetKernelTimeUs() const;

	double GetCpuTimeMs() const;
	double GetUserTimeMs() const;
	double GetKernelTimeMs() const;
private:
	uint64_t gCpuTimeNs;
	uint64_t gUserTimeNs;
	uint64_t gKernelTimeNs;

};

