// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#ifdef __unix__         
	#define LINUX
#elif defined(_WIN32) || defined(WIN32) 
	#define WINDOWS
#endif

#ifdef WINDOWS
	#include "targetver.h"
	#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
	#include <Windows.h>
	#include "psapi.h"
#elif defined(LINUX)
	#include <sys/time.h>
	#include <sys/resource.h>
	#include <sys/types.h>
	#include <sys/sysinfo.h>
	#include <stdlib.h>
	#include <stdio.h>
	#include <unistd.h>
	#include <errno.h>
	#include <string.h>
#endif // _WIN32

#include <cstdint>
#include <memory>
#include <cmath>
// TODO: reference additional headers your program requires here
