#pragma once
class Profiler
{
public:
	Profiler();
	virtual ~Profiler();
	virtual void StartProfiling(void) throw (std::exception)=0;
	virtual void StopProfiling(void) throw (std::exception)=0;
protected:
	virtual void ResetValues(void) = 0;//pure virtual functions
private:
	
};

