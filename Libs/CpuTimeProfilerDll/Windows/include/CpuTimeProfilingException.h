#pragma once
#include <iostream>
#include <exception>

#ifdef WINDOWS
	#ifdef CPUTIMEPROFILERDLL_EXPORTS
		#define CPUTIMEPROFILERDLL_API __declspec(dllexport)
	#else
		#define CPUTIMEPROFILERDLL_API __declspec(dllimport)
	#endif
#elif defined(LINUX)
	#define CPUTIMEPROFILERDLL_API 
#endif // _WIN32

// This class is exported from the CpuTimeProfilerDll.dll
class CPUTIMEPROFILERDLL_API CpuTimeProfilingException :public std::exception
{
public:
	CpuTimeProfilingException(const std::string msg) :g_msg(msg)
	{

	}
	CpuTimeProfilingException() :g_msg("CpuTimeProfilingException"){}
	const char *what() const throw()
	{
		return g_msg.c_str();
	}
private:
	std::string g_msg;
};