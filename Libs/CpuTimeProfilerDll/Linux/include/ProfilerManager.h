#ifdef WINDOWS
	#ifdef CPUTIMEPROFILERDLL_EXPORTS
		#define CPUTIMEPROFILERDLL_API __declspec(dllexport)
	#else
		#define CPUTIMEPROFILERDLL_API __declspec(dllimport)
	#endif
#elif defined(LINUX)
	#define CPUTIMEPROFILERDLL_API 
#endif // _WIN32
enum CPUTIMEPROFILERDLL_API E_ProfilingOption :uint8_t
{
	OptionMemoryUsage,
	OptionCpuTime
};
// This class is exported from the CpuTimeProfilerDll.dll
class CPUTIMEPROFILERDLL_API ProfilerManager
{
public:
	ProfilerManager();
	virtual ~ProfilerManager();
	void RunProfiler(E_ProfilingOption option);
	void PreprocessResultsAndStop();
	bool IsError() const;
	std::string GetErrorMessage() const;
	CpuTimeValues GetCpuTimeProfilerResults() const;
	MemoryValues GetMemoryUsage() const;
protected:
private:
	bool gIsCpuProfilerStarted;
	bool gIsMemProfilerStarted;
	std::unique_ptr<Profiler> gCpuTimeProfiler;
	std::unique_ptr <Profiler> gMemoryProfiler;
	std::string gErrorString;
	
};

