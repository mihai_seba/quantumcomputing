#pragma once
#include "Profiler.h"
class CpuTimeProfiler:public Profiler {
public:
	CpuTimeProfiler(void);
	virtual ~CpuTimeProfiler();
	void StartProfiling(void) throw (std::exception);
	void StopProfiling(void) throw (std::exception);
	void FillProfilerValues(CpuTimeValues *cpuTimeValues);
protected:
	void ResetValues();
private:
	void CalculateCpuTime() throw (std::exception);
#ifdef WINDOWS
	void GetWindowsCpuTime(const uint64_t initUserTime, const uint64_t initKernelTime, const uint64_t initCpuTime) throw (std::exception);
#elif defined(LINUX)
	void GetLinuxCpuTime(const uint64_t initUserTime, const uint64_t initKernelTime, const uint64_t initCpuTime) throw (std::exception);
#endif
	uint64_t gCpuTime;
	uint64_t gUserTime;
	uint64_t gKernelTime;
	bool gIsCpuProfilingStarted;
};




