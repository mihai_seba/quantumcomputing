#include "stdafx.h"
#include "Individual.h"
#include "FitnessCalculator.h"
#include "Population.h"


Population::Population()
{
	gPopulation = std::vector<std::shared_ptr<Individual>>();
}


Population::~Population()
{
	gPopulation.clear();
}

void Population::SetPopulationSize(const size_t size){
	gPopulationSize = size;
	gPopulation.reserve(gPopulationSize);
	for (size_t i = 0; i < gPopulationSize; i++){
		gPopulation.push_back(nullptr);
	}
}
size_t Population::GetPopulationSize() const{
	return gPopulationSize;
}

void Population::Initialize(){
	FitnessCalculator *calculator = &FitnessCalculator::GetInstance();
	for (size_t i = 0; i < gPopulationSize; i++){
		std::shared_ptr<Individual> individual = std::make_shared<Individual>();
		individual->GenerateIndividual();
		calculator->CalculateFitness(individual);
		gPopulation[i]=individual;
	}
}

std::shared_ptr<Individual> Population::GetIndividual(const size_t idx) const{
	return gPopulation.at(idx);
}

std::shared_ptr<Individual> Population::GetFittestIndividual() const{
	std::shared_ptr<Individual> fittestIndividual = gPopulation.at(0);
	
	for (std::shared_ptr<Individual> ind : gPopulation){
		if (fittestIndividual->GetFitnessValue() < ind->GetFitnessValue()){
			fittestIndividual = ind;
		}
	}
	return fittestIndividual;
}

void Population::SaveIndividual(const int idx, const std::shared_ptr<Individual> individual){
	gPopulation[idx] = individual;
}