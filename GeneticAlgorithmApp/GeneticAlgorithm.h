#pragma once
class GeneticAlgorithm
{
    public:
		GeneticAlgorithm();
		virtual ~GeneticAlgorithm();
		void EvolvePopulation();
		void SetPopulation(std::shared_ptr<Population>population);
		std::shared_ptr<Population>GetPopulation();
    protected:
    private:
		std::shared_ptr<Individual> CrossOver(const Individual &x, const Individual &y);
		void Mutate(std::shared_ptr<Individual> individual);
		std::shared_ptr<Individual> TournamentSelection();
		std::shared_ptr<Population>gPopulation;
		const double UNIFORM_RATE = 0.7;
		const double MUTATION_RATE = 0.015;
		const size_t TOURNAMENT_SIZE = 5;
		const bool ELITISM = true;
};