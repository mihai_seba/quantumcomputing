#pragma once
class FitnessCalculator
{
public:
	static FitnessCalculator &GetInstance();
	inline virtual ~FitnessCalculator(){}
	
	
	void SetSolution(const std::vector<uint8_t> solution);
	void CalculateFitness(std::shared_ptr<Individual> individual);
	uint32_t GetMaximumFitness() const;
private:
	static std::unique_ptr<FitnessCalculator> instance_;
	static std::once_flag gOnceFlag;
	inline FitnessCalculator(){}
	FitnessCalculator(const FitnessCalculator& rs) = delete;
	FitnessCalculator& operator = (const FitnessCalculator& rs) = delete;
	std::vector<uint8_t> gSolution;
};

