#include "stdafx.h"
#include "Individual.h"
#include "Population.h"
#include "FitnessCalculator.h"
#include "GeneticAlgorithm.h"

GeneticAlgorithm::GeneticAlgorithm(){
	
}

GeneticAlgorithm::~GeneticAlgorithm(){

}

void GeneticAlgorithm::SetPopulation(std::shared_ptr<Population>population){
	gPopulation = population;
}
std::shared_ptr<Population>GeneticAlgorithm::GetPopulation(){
	return gPopulation;
}
void GeneticAlgorithm::EvolvePopulation()
{
	size_t elitismOffset;
	size_t populationSize = gPopulation->GetPopulationSize();
	std::shared_ptr<Population> newPopulation = std::make_shared<Population>();
	newPopulation->SetPopulationSize(populationSize);
	FitnessCalculator *calculator = &FitnessCalculator::GetInstance();
	/*save fittest individual*/
	if (ELITISM){
		newPopulation->SaveIndividual(0, gPopulation->GetFittestIndividual());
		elitismOffset = 1;
	}
	else{
		elitismOffset = 0;
	}
	//crossover
	for (size_t i = elitismOffset; i < populationSize; i++){
		std::shared_ptr<Individual> individualOne = TournamentSelection();//select 1 fittest individual from 5 random chosen
		std::shared_ptr<Individual> individualTwo = TournamentSelection();//select 1 fittest individual from 5 random chosen
		std::shared_ptr<Individual> newIndividual = CrossOver(*individualOne.get(), *individualTwo.get());
		calculator->CalculateFitness(newIndividual);
		newPopulation->SaveIndividual(i, newIndividual);
	}

	/*Mutate*/
	for (size_t i = elitismOffset; i < populationSize; i++){
		Mutate(newPopulation->GetIndividual(i));
	}
	gPopulation = newPopulation;
}
std::shared_ptr<Individual> GeneticAlgorithm::CrossOver(const Individual &x, const Individual &y){
	std::random_device rd; // obtain a random number from hardware
	std::mt19937 eng(rd()); // seed the generator
	std::uniform_real_distribution<> distr(0, 1);// define the range
		
	std::shared_ptr<Individual>newIndividual = std::make_shared<Individual>();
	for (size_t i = 0; i < Individual::DEFAULT_GENE_LENGTH; i++){
		if (distr(eng) <= UNIFORM_RATE){
			newIndividual->SetGene(i, x.GetGene(i));
		}
		else{
			newIndividual->SetGene(i, y.GetGene(i));
		}
	}
	return newIndividual;
}
void GeneticAlgorithm::Mutate(std::shared_ptr<Individual> individual){
	std::random_device rd; // obtain a random number from hardware
	std::mt19937 eng(rd()); // seed the generator
	std::uniform_real_distribution<> distr(0, 1); // define the range
	for (size_t i = 0; i < Individual::DEFAULT_GENE_LENGTH; i++){
		double value = distr(eng);
		if (value <= MUTATION_RATE){
			uint8_t gene = std::round(value);
			individual->SetGene(i, gene);
		}
	}

}
std::shared_ptr<Individual> GeneticAlgorithm::TournamentSelection(){
	std::shared_ptr<Population> newPopulation = std::make_shared<Population>();
	newPopulation->SetPopulationSize(TOURNAMENT_SIZE);
	
	std::random_device rd; // obtain a random number from hardware
	std::mt19937 eng(rd()); // seed the generator
	std::uniform_int_distribution<> distr(0, (gPopulation->GetPopulationSize()-1)); // define the range
	for (size_t i = 0; i < TOURNAMENT_SIZE; i++){
		size_t randomIndividual = distr(eng);
		newPopulation->SaveIndividual(i, gPopulation->GetIndividual(randomIndividual));
		
		
	}
	return newPopulation->GetFittestIndividual();
}