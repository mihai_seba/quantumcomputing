#pragma once
class  Individual
{
	public:
		Individual();
		virtual ~Individual();
		void GenerateIndividual();
		uint32_t GetFitnessValue() const;
		void SetFitnessValue(const uint32_t fitnessValue);
		uint32_t GetChromosomeValue() const;
		uint8_t GetGene(size_t idx) const;
		void SetGene(const size_t idx, const uint8_t gene);
		static const size_t DEFAULT_GENE_LENGTH;
		std::string to_string() const;
		friend std::ostream& operator<<(std::ostream& os, const Individual& obj)
		{
			os << obj.to_string();
			return os;
		}
	protected:
	private:	
		void Initialize();
		uint32_t gFitnessValue;
		uint32_t gChromosomeValue;
		
		
		std::vector<uint8_t> gGenes;
};


