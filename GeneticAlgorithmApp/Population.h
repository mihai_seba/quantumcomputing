#pragma once
class Population
{
public:
	Population();
	~Population();
	void Initialize();
	void SetPopulationSize(const size_t size);
	size_t GetPopulationSize() const;
	std::shared_ptr<Individual> GetIndividual(const size_t idx) const;
	std::shared_ptr<Individual> GetFittestIndividual() const;
	void SaveIndividual(const int idx, const std::shared_ptr<Individual> individual);
private:
	std::vector<std::shared_ptr<Individual>> gPopulation;
	size_t gPopulationSize;
};

