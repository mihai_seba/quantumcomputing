#include "stdafx.h"
#include "Individual.h"
#include "FitnessCalculator.h"

std::unique_ptr<FitnessCalculator>FitnessCalculator::instance_;
std::once_flag FitnessCalculator::gOnceFlag;

FitnessCalculator &FitnessCalculator::GetInstance()
{
#ifdef LINUX
	if (instance_ == NULL){
		instance_ = std::unique_ptr<FitnessCalculator>(new FitnessCalculator);
	}
#else
	std::call_once(gOnceFlag,
		[] {
		instance_.reset(new FitnessCalculator);
	});
#endif
	return *instance_.get();
}

void FitnessCalculator::SetSolution(const std::vector<uint8_t> solution){
	gSolution = solution;
}

void FitnessCalculator::CalculateFitness(std::shared_ptr<Individual> individual){
	uint32_t fitness = 0;
	for (size_t i = 0; i < Individual::DEFAULT_GENE_LENGTH; i++){
		if (individual->GetGene(i) == gSolution.at(i)){
			fitness++;
		}
	}
	(*individual).SetFitnessValue(fitness);
}

uint32_t FitnessCalculator::GetMaximumFitness() const {
	return gSolution.size();
}