#include "stdafx.h"
#include "Individual.h"

const size_t Individual::DEFAULT_GENE_LENGTH = 4;//32;//64;

Individual::Individual(){
	gGenes.reserve(DEFAULT_GENE_LENGTH);
	Initialize();
}

Individual::~Individual(){
	gGenes.clear();
}

void Individual::Initialize(){
	for (size_t i = 0; i < DEFAULT_GENE_LENGTH; i++){
		gGenes.push_back(0);
	}
}

void Individual::GenerateIndividual(){
	std::random_device rd; // obtain a random number from hardware
	std::mt19937 eng(rd()); // seed the generator
	std::uniform_int_distribution<> distr(0, 1); // define the range
	for (size_t i = 0; i < DEFAULT_GENE_LENGTH; ++i){
		gGenes[i]=distr(eng);
	}

}

std::string Individual::to_string() const{
	std::stringstream strStream;
	for (uint8_t gene : gGenes){
		strStream << std::bitset<1>(gene).to_string();
		
	}
	return strStream.str();
}

uint8_t Individual::GetGene(size_t idx) const {
	return gGenes.at(idx);
}

void Individual::SetGene(const size_t idx, const uint8_t gene){
	gGenes[idx] = gene;
}

uint32_t Individual::GetFitnessValue() const{
	return gFitnessValue;
}

void Individual::SetFitnessValue(const uint32_t fitnessValue){
	gFitnessValue = fitnessValue;
}

uint32_t Individual::GetChromosomeValue() const{
	return gChromosomeValue;
}

