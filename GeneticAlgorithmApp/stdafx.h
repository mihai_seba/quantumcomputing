// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#pragma region Unix/Windows Portability guards
#ifdef __unix__         
    #define LINUX
#elif defined(_WIN32) || defined(WIN32) 
    #define WINDOWS

#endif
#ifdef WINDOWS
    #include "targetver.h"
    #define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#endif // _WIN32
#pragma endregion

#include <memory>
#include <iostream>
#include <mutex>
#include <complex>
#include <algorithm>
#include <vector>
#include <exception>
#include <cmath>
#include <cstdlib>
#include <cstdint>
#include <random>
#include <bitset>
#include <cstdlib>








