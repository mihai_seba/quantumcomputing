// GeneticAlgorithmApp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Individual.h"
#include "FitnessCalculator.h"
#include "Population.h"
#include "GeneticAlgorithm.h"

int main(int argc, char** argv)
{
	size_t generationCount = 0;
	FitnessCalculator *calculator = &FitnessCalculator::GetInstance();
	std::vector<uint8_t> solution({0,1,0,1});
	
	//{ 1, 0, 0, 1, 1, 0, 0, 1,
	//								0, 0, 0, 0, 0, 1, 1, 0,
	//								1, 1, 1, 1, 1, 0, 0, 0,
	//								0, 0, 0, 0, 0, 0, 0, 0});//used to count bits
	//{ 1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1 });
	
	
	calculator->SetSolution(solution);
	uint32_t maximumFitness = calculator->GetMaximumFitness();
	std::shared_ptr<Population> population = std::make_shared<Population>();
	std::unique_ptr<GeneticAlgorithm> ga(new GeneticAlgorithm);
	population->SetPopulationSize(50);
	population->Initialize();
	
	do{
		ga->SetPopulation(population);
		generationCount++;
		std::cout << "\nGeneration:" << generationCount << ' ' << "Fitness:" << population->GetFittestIndividual()->GetFitnessValue();
		ga->EvolvePopulation();
		population = ga->GetPopulation();
	} while (population->GetFittestIndividual()->GetFitnessValue() < maximumFitness);
	std::cout << "\nSolution Found!\n";
	std::cout << "Generation:" << generationCount;
	std::cout << "Genes:" << population->GetFittestIndividual()->to_string();
	return 0;
}

