#pragma once
class RQGaAlgorithm
{
public:
	RQGaAlgorithm();
	virtual ~RQGaAlgorithm();
	void InitializePopulation();
	void RunAlgorithm();
	void PrintResult();
protected:
private:
	void Measure();
	void CalculateSolution();
	std::vector<Individual> gPopulation;
	std::shared_ptr<Oracle>gOracleObj;
	ConfigurationUtils *gConfiguration;
	IndividualManager *gIndividualManager;
	std::shared_ptr<GroverImplementation> gGroverImplementation;
	uint32_t gMaxValue;
	QRegisterOperations *gRegisterOperations;
	size_t gRegisterMaxValueIndex;
};

