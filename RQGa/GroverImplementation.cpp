#include "stdafx.h"
#include "ConfigurationUtils.h"
#include "Individual.h"
#include "IndividualManager.h"
#include "Oracle.h"
#include "GroverImplementation.h"




GroverImplementation::GroverImplementation() :QAlgorithms()
{



}




GroverImplementation::~GroverImplementation()
{
}



void GroverImplementation::Init()
{
	gGateH = gGateFactory.CreateGate(EHadamardGate);
	gGateX = gGateFactory.CreateGate(EXGate);
	gGateCNot = gGateFactory.CreateGate(ECnotGate);
	gGateCPhaseShift = gGateFactory.CreateGate(ECPhaseShift);

	//consider each register from search space a qubit;
	gQregister = QRegister();
	gQregister.reserve_space(ConfigurationUtils::NUMBER_OF_INDIVIDUALS + 1);
	gRegOps = &QRegisterOperations::GetInstance();
	for (size_t i = 0; i < ConfigurationUtils::NUMBER_OF_INDIVIDUALS; i++){
		gQregister.push(gSearchSpace.at(i));
	}
	gQregister.push(QubitOne());
	
	gResultQubit = gQregister();
	
	SetOracleObj(gOracle);
	SetOracleMethod(&Oracle::ApplyOracle, (ConfigurationUtils::NUMBER_OF_INDIVIDUALS + 1));


}

void GroverImplementation::SetOracle(std::shared_ptr<Oracle>oracle)
{
	gOracle = oracle;
}

void GroverImplementation::SetSearchSpace(std::vector<Qubit> searchSpace)
{
	gSearchSpace = searchSpace;
}

void GroverImplementation::Run()
{
	
	std::vector<size_t>affectedPositions;
	
	 
	for (size_t index = 0; index < ConfigurationUtils::NUMBER_OF_INDIVIDUALS; index++){
	
		affectedPositions.push_back(index);
	
	}
	
		
	//apply first stage -> hadamards
	affectedPositions.push_back(affectedPositions.back() + 1);
	gGateH->Apply(&gResultQubit, affectedPositions, (ConfigurationUtils::NUMBER_OF_INDIVIDUALS + 1));
	//{ 0, 1, 2, 3, 4 }, (ConfigurationUtils::NUMBER_OF_INDIVIDUALS + 1));
	//apply oracle
	ApplyOracle();

	//apply hadamards
	affectedPositions.pop_back();
	gGateH->Apply(&gResultQubit, affectedPositions, (ConfigurationUtils::NUMBER_OF_INDIVIDUALS + 1));
		//{ 0, 1, 2, 3 }, (ConfigurationUtils::NUMBER_OF_INDIVIDUALS + 1));

	//apply X gates
	gGateX->Apply(&gResultQubit, affectedPositions, (ConfigurationUtils::NUMBER_OF_INDIVIDUALS + 1));
	//{ 0, 1, 2, 3 }, (ConfigurationUtils::NUMBER_OF_INDIVIDUALS + 1));

	//apply Phase shift
	affectedPositions.pop_back();
	gGateCPhaseShift->Apply(&gResultQubit, affectedPositions.back() + 1, affectedPositions, (ConfigurationUtils::NUMBER_OF_INDIVIDUALS + 1));
		//3, { 0, 1, 2 }, (ConfigurationUtils::NUMBER_OF_INDIVIDUALS + 1));

	//apply X gates
	affectedPositions.push_back(affectedPositions.back() + 1);
	gGateX->Apply(&gResultQubit, affectedPositions, (ConfigurationUtils::NUMBER_OF_INDIVIDUALS + 1));
	//{ 0, 1, 2, 3 }, (ConfigurationUtils::NUMBER_OF_INDIVIDUALS + 1));

	affectedPositions.push_back(affectedPositions.back() + 1);
	gGateH->Apply(&gResultQubit, affectedPositions, (ConfigurationUtils::NUMBER_OF_INDIVIDUALS + 1));
	//{ 0, 1, 2, 3, 4 }, (ConfigurationUtils::NUMBER_OF_INDIVIDUALS + 1));



}

void GroverImplementation::Measure()
{



	MeasurementPerformer measure = MeasurementPerformer(100);
	measure.Configure(gResultQubit);
	gResultQubit = measure.Measure();

}

void GroverImplementation::ExtractResult(size_t *value)
{
	size_t size = gResultQubit.GetQubitStates().size();
	size_t position = 0;
	size_t intervalSize = size / ConfigurationUtils::NUMBER_OF_INDIVIDUALS;
	
	//std::cout << "\nSize:" << size;
	for (size_t i = 0; i < size;i++){
		if (gResultQubit.GetQubitStates()[i] == std::complex<double>(1.0, 0.0)){
			position = i;
			break;
		}
	}
	//std::cout << "\nInterval size:" << intervalSize;
	//std::cout << "\nPosition:" << position;

	//calculate selected fitness register :)
	for (size_t i = 0; i < ConfigurationUtils::NUMBER_OF_INDIVIDUALS; i++){
		if (position >= (i*intervalSize) && position < ((i + 1) * intervalSize)){
			*value = i;
			break;
		}
	}
	

}