#pragma once
class GroverImplementation :public QAlgorithms
{
public:
	GroverImplementation();
	virtual ~GroverImplementation();

#pragma region Inherited methods	
	void Init();
	void Run();
	void Measure();
#pragma endregion
	void SetSearchSpace(std::vector<Qubit> searchSpace);
	void SetOracle(std::shared_ptr<Oracle>oracle);
	void ExtractResult(size_t *value);
private:
#pragma region Grovers Algorithm fields
	
	
	std::shared_ptr<IGate> gGateH;
	std::shared_ptr<IGate> gGateX;
	std::shared_ptr<IGate> gGateCNot;
	std::shared_ptr<IGate> gGateCPhaseShift;
	
	std::vector<Qubit> gSearchSpace;
	std::shared_ptr<Oracle> gOracle;
	QRegisterOperations *gRegOps;
#pragma endregion
	size_t gNumberOfQubits;
};
