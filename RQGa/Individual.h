#pragma once
class Individual
{
public:
	Individual();
	virtual ~Individual();
	QRegister &GetFitnessRegister();
	QRegister &GetChromosomeRegister();
	void SetFitnessRegister(QRegister *qRegister);
	void SetChromosomeRegister(QRegister *qRegister);
	void SetFitnessValue(const double fitnessValue);
	double GetFitnessValue() const;
	void CalculateValues();
	bool IsValid();
#pragma region Application specific methods
	uint32_t GetValue() const;
	uint32_t GetCapacity() const;
	void EntangleFitnessRegister();
	Qubit GetEntangledFitnessRegister() const;
#pragma endregion
private:
	void InitializeRegisters();
	Qubit gEntangledFitnessRegister;
	std::shared_ptr<QRegister> gFitnessRegister;
	std::shared_ptr<QRegister> gChromosomeRegister;
	ConfigurationUtils *gConfigurationUtils;
	QRegisterOperations *gRegisterOperations;
#pragma region Application specific fields
	uint32_t value;
	uint32_t capacity;
	double gFitnessValue;
#pragma endregion
};

