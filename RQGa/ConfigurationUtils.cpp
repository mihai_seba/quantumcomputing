#include "stdafx.h"
#include "ConfigurationUtils.h"


std::unique_ptr<ConfigurationUtils>ConfigurationUtils::instance_;
std::once_flag ConfigurationUtils::gOnceFlag;

const std::vector<std::string> ConfigurationUtils::FILL_PATTERNS = {
	/*"00",
	"01",
	"10",
	"11"*/
	"0000",
	"0001",
	"0010",
	"0011",
	"0100",
	"0101",
	"0110",
	"0111",
	"1000",
	"1001",
	"1010",
	"1011",
	"1100",
	"1101",
	"1110",
	"1111"
};

const size_t ConfigurationUtils::NUMBER_OF_INDIVIDUALS = ConfigurationUtils::FILL_PATTERNS.size();




ConfigurationUtils &ConfigurationUtils::GetInstance()
{
#ifdef LINUX
	if(instance_==NULL){
		instance_=std::unique_ptr<ConfigurationUtils>(new ConfigurationUtils);
	}
#else
	std::call_once(gOnceFlag,
		[] {
		instance_.reset(new ConfigurationUtils);
	});
#endif
	return *instance_.get();
}

