// RQGa.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ConfigurationUtils.h"
#include "Individual.h"
#include "IndividualManager.h"
#include "GroverImplementation.h"
#include "RQGaAlgorithm.h"
#include <algorithm>
#include <thread>
#include <chrono>
#include <functional>
#include <atomic>



int main(int argc, char* argv[])
{

	
		

		std::unique_ptr<RQGaAlgorithm> rqgaAlgorithm(new RQGaAlgorithm);
		rqgaAlgorithm->InitializePopulation();
		rqgaAlgorithm->RunAlgorithm();
		rqgaAlgorithm->PrintResult();
		

		
		




	return 0;
}

