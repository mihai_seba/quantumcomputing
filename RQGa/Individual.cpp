#include "stdafx.h"
#include "ConfigurationUtils.h"
#include "Individual.h"



Individual::Individual()
{
	this->value = 0;
	this->capacity = 0;
	InitializeRegisters();
}


Individual::~Individual()
{
}

void Individual::InitializeRegisters()
{
	//get instance of ConfigurationUtils to access application constants;
	gConfigurationUtils = &ConfigurationUtils::GetInstance();
	//get instance of QRegisterOperations to access QRegister operations
	gRegisterOperations = &QRegisterOperations::GetInstance();
#pragma region Chrmosome register initialization
	//create chromosome quantum register
	gChromosomeRegister = std::shared_ptr<QRegister>(new QRegister);
	//reserve space for chromosome register
	gChromosomeRegister->reserve_space(gConfigurationUtils->CHROMOSOME_REGISTER_SIZE);
	//fill chromosome register with zeroes
	gRegisterOperations->FillWithZero(gChromosomeRegister.get());
#pragma endregion

#pragma region Fitness register initialization
	//create fitness quantum register
	gFitnessRegister = std::shared_ptr<QRegister>(new QRegister);
	//reserve space for fitness register
	gFitnessRegister->reserve_space(gConfigurationUtils->FITNESS_REGISTER_SIZE);
	//fill fitness register with zeroes
	gRegisterOperations->FillWithZero(gFitnessRegister.get());
#pragma endregion
}
bool Individual::IsValid()
{
	if (capacity == 0){
		CalculateValues();
	}
	return (capacity <= ConfigurationUtils::GetInstance().MAXIMUM_ALLOWED_PACKAGE_MASS);
}
QRegister &Individual::GetFitnessRegister()
{
	return *gFitnessRegister.get();
}
QRegister &Individual::GetChromosomeRegister()
{
	return *gChromosomeRegister.get();
}

void Individual::SetFitnessRegister(QRegister *qRegister)
{
	gFitnessRegister.reset(qRegister);
}
void Individual::SetChromosomeRegister(QRegister *qRegister)
{
	gChromosomeRegister.reset(qRegister);
}

void Individual::EntangleFitnessRegister()
{
	//TODO:Entangle fitness except 1st qubit
	QRegister originalFitness = GetFitnessRegister();
	QRegister newQuantumRegister = QRegister(originalFitness.size() - 1);
	gRegisterOperations->CopyRegister(&newQuantumRegister, &originalFitness, 1);
	
	
	gEntangledFitnessRegister = newQuantumRegister();
}
Qubit Individual::GetEntangledFitnessRegister() const
{
	return gEntangledFitnessRegister;
}


void Individual::CalculateValues()
{
	capacity = 0;
	value = 0;
	for (size_t i = 0; i < gChromosomeRegister->size(); i++){
		if (gChromosomeRegister->at(i) == QubitOne()){
			switch (i)
			{
			case 0:
				// I1 (7 kg 40 $)
				capacity += 7;
				value += 4;
				break;
			case 1:
				// I2 (4 kg, 100 $)
				capacity += 4;
				value += 5;
				break;
			case 2:
				// I3 (2 kg, 50 $)
				capacity += 2;
				value += 5;
				break;
			case 3:
				// I4 (3 kg, 30)
				capacity += 3;
				value += 3;
				break;
			default:
				break;
			}
			//switch (i)
			//{
			//case 0:
			//	// I1 (7 kg 40 $)
			//	capacity += 7;
			//	value += 4;
			//	break;
			//case 1:
			//	// I2 (4 kg, 100 $)
			//	capacity += 4;
			//	value += 9;
			//	break;
			//case 2:
			//	// I3 (2 kg, 50 $)
			//	capacity += 2;
			//	value += 5;
			//	break;
			//case 3:
			//	// I4 (3 kg, 30)
			//	capacity += 3;
			//	value += 3;
			//	break;
			//default:
			//	break;
			//}
		}
	}
}

uint32_t Individual::GetValue() const {
	return value;
}
uint32_t Individual::GetCapacity() const {
	return capacity;
}

void Individual::SetFitnessValue(const double fitnessValue)
{
	gFitnessValue = fitnessValue;
}

double Individual::GetFitnessValue() const{
	return gFitnessValue;
}