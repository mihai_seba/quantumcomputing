#pragma once
class Oracle
{
public:
	Oracle();
	virtual ~Oracle();

	void ApplyOracle(Qubit *qubit, size_t registerSize);
	void SetPopulation(std::vector<Individual> population);
	void SetFitnessMaxValue(uint32_t fitnessMaxValue);
private:
	GateFactory gGateFactory;
	std::shared_ptr<IGate> gGateX;
	std::shared_ptr<IGate> gGateCNot;
	std::vector<Individual> gPopulationObj;
	uint32_t gFitnessMaxValue;
};

