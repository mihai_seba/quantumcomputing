#pragma once
class Population
{
public:
	Population();
	~Population();
	void Initialize();
	void SetPopulationSize(const size_t size);
	size_t GetPopulationSize() const;
	std::shared_ptr<GaIndividual> GetIndividual(const size_t idx) const;
	std::shared_ptr<GaIndividual> GetFittestIndividual() const;
	void SaveIndividual(const int idx, const std::shared_ptr<GaIndividual> individual);
private:
	std::vector<std::shared_ptr<GaIndividual>> gPopulation;
	size_t gPopulationSize;
};

