#include "stdafx.h"
#include "GaIndividual.h"
#include "Population.h"
#include "ValuesGenerator.h"
#include "FitnessCalculator.h"
#include "GaConfigurationManager.h"
#include "EvolutionFactor.h"

EvolutionFactor::EvolutionFactor()
{
	gRandomValueGenerator = &ValuesGenerator::GetInstance();
	gConfigurationManager = &GaConfigurationManager::GetInstance();
	
}
EvolutionFactor::~EvolutionFactor()
{

}


void EvolutionFactor::EvolvePopulation()
{
	size_t elitismOffset;
	size_t populationSize = gPopulation->GetPopulationSize();
	std::shared_ptr<Population> newPopulation = std::make_shared<Population>();
	newPopulation->SetPopulationSize(populationSize);
	std::shared_ptr<FitnessCalculator> fitnessCalculator = std::make_shared<FitnessCalculator>();

	fitnessCalculator->SetMaximumValue(gConfigurationManager->GetIndividualMaxFitnessValue());
	/*save fittest individual*/
	if (ELITISM){
		newPopulation->SaveIndividual(0, gPopulation->GetFittestIndividual());
		elitismOffset = 1;
	}
	else{
		elitismOffset = 0;
	}
	for (size_t i = elitismOffset; i < populationSize; i++){
		std::shared_ptr<GaIndividual> individualOne = TournamentSelection();
		std::shared_ptr<GaIndividual> individualTwo = TournamentSelection();
		std::shared_ptr<GaIndividual> newIndividual = CrossOver(*individualOne.get(), *individualTwo.get());


		fitnessCalculator->CalculateFitness(newIndividual);
		newPopulation->SaveIndividual(i, newIndividual);
	}

	/*Mutate*/
	for (size_t i = elitismOffset; i < populationSize; i++){
		Mutate(newPopulation->GetIndividual(i));
	}
	gPopulation = newPopulation;
}

void EvolutionFactor::SetPopulation(std::shared_ptr<Population>population){
	gPopulation = population;
}
std::shared_ptr<Population>EvolutionFactor::GetNewGeneration(){
	return gPopulation;
}

std::shared_ptr<GaIndividual> EvolutionFactor::CrossOver(const GaIndividual &x, const GaIndividual &y){
	double uniformRate = 0.0;


	std::shared_ptr<GaIndividual>newIndividual = std::make_shared<GaIndividual>();
	newIndividual->Initialize(gConfigurationManager->GetGeneLength(), gConfigurationManager->GetQRegisterSize());
			
	for (size_t i = 0; i < gConfigurationManager->GetGeneLength(); i++){
		gRandomValueGenerator->GenerateUniformRealValue(0, 1, &uniformRate);
		//if (std::fabs(uniformRate - UNIFORM_RATE) < TOLERANCE))
		std::vector<int8_t> array = newIndividual->GetGenome();
		if (uniformRate <= UNIFORM_RATE){
			if (std::find(array.begin(), array.end(), x.GetGene(i)) == array.end())
			{
				newIndividual->SetGene(i, x.GetGene(i));

			}

		}
		else{
			if (std::find(array.begin(), array.end(), y.GetGene(i)) == array.end())
			{
				newIndividual->SetGene(i, y.GetGene(i));

			}


		}
	}
	return newIndividual;
}
void EvolutionFactor::Mutate(std::shared_ptr<GaIndividual> individual){
	double value;
	int8_t gene;
	for (size_t i = 0; i < gConfigurationManager->GetGeneLength(); i++){
		gRandomValueGenerator->GenerateUniformRealValue(0, 1, &value);
		//if (std::fabs(uniformRate - UNIFORM_RATE) < TOLERANCE))
		if (value <= MUTATION_RATE){
			gRandomValueGenerator->GenerateGeneValue(individual->GetGeneMaxLength(), individual->GetGenome(), &gene);
			individual->SetGene(i, gene);

		}
	}

}
std::shared_ptr<GaIndividual> EvolutionFactor::TournamentSelection(){
	std::shared_ptr<Population> newPopulation = std::make_shared<Population>();
	newPopulation->SetPopulationSize(TOURNAMENT_SIZE);
	size_t randomIndividual;
	for (size_t i = 0; i < TOURNAMENT_SIZE; i++){
		gRandomValueGenerator->GenerateUniformIntegerValue(0, (gPopulation->GetPopulationSize() - 1), &randomIndividual);
		newPopulation->SaveIndividual(i, gPopulation->GetIndividual(randomIndividual));


	}
	return newPopulation->GetFittestIndividual();
}