#include "stdafx.h"
#include "GaIndividual.h"
#include "Population.h"
#include "ValuesGenerator.h"
#include "FitnessCalculator.h"
#include "GaConfigurationManager.h"
#include "EvolutionFactor.h"
#include "GeneticAlgorithm.h"

GeneticAlgorithm::GeneticAlgorithm(){
	
}

GeneticAlgorithm::~GeneticAlgorithm(){

}



void GeneticAlgorithm::RunAlgorithm()
{
	size_t generationCount = 0;
	gPopulation = std::make_shared<Population>();
	std::shared_ptr<EvolutionFactor> generationEvolver = std::make_shared<EvolutionFactor>();
	gPopulation->SetPopulationSize(GaConfigurationManager::GetInstance().GetPopulationSize());
	gPopulation->Initialize();
	//std::cout << "\nGeneration:" << generationCount << ' ' << "Fitness:" << population->GetFittestIndividual()->GetFitnessValue();
	do{
		generationCount++;
		generationEvolver->SetPopulation(gPopulation);
		generationEvolver->EvolvePopulation();
		gPopulation = generationEvolver->GetNewGeneration();
		//std::cout << "\nGeneration:" << generationCount << ' ' << "Fitness:" << population->GetFittestIndividual()->GetFitnessValue();
	} while (gPopulation->GetFittestIndividual()->GetFitnessValue() < GaConfigurationManager::GetInstance().GetGeneLength());

	//std::cout << "\nSolution Found!\n";
	//std::cout << "Generation:" << generationCount;
	//std::cout << "Genes:" << population->GetFittestIndividual()->to_string();
}

std::vector<int8_t> GeneticAlgorithm::GetGenome() const
{
	return gPopulation->GetFittestIndividual()->GetGenome();
}