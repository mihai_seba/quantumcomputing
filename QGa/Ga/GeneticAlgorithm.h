#pragma once
class GeneticAlgorithm
{
    public:
		GeneticAlgorithm();
		virtual ~GeneticAlgorithm();
		

		void RunAlgorithm();
		std::vector<int8_t> GetGenome() const;
		
    protected:
    private:
		
		std::vector<int8_t> gGenome;
		std::shared_ptr<Population> gPopulation;
};