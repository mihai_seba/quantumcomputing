#pragma once
class GaConfigurationManager
{
public:
	static GaConfigurationManager &GetInstance();
	
	virtual ~GaConfigurationManager();
	void SetQRegisterSize(const size_t qRegisterSize);
	void SetGeneLength(const size_t geneLength);
	void SetPopulationSize(const size_t populationSize);
	void SetIndividualMaxFitnessValue(const uint32_t maxValue);
	
	size_t GetQRegisterSize() const;
	size_t GetGeneLength() const;
	size_t GetPopulationSize() const;
	uint32_t GetIndividualMaxFitnessValue()const;
protected:
	
private:
	GaConfigurationManager();
	GaConfigurationManager(const GaConfigurationManager& rs) = delete;
	GaConfigurationManager& operator = (const GaConfigurationManager& rs) = delete;
	static std::unique_ptr<GaConfigurationManager> instance_;
	static std::once_flag gOnceFlag;

	uint32_t gIndividualMaxValue;
	size_t gQRegisterSize;
	size_t gGeneLength;
	size_t gPopulationSize;

	
};