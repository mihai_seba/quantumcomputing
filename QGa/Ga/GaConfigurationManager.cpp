#include "stdafx.h"
#include "GaConfigurationManager.h"

std::unique_ptr<GaConfigurationManager>GaConfigurationManager::instance_;
std::once_flag GaConfigurationManager::gOnceFlag;

GaConfigurationManager &GaConfigurationManager::GetInstance()
{
#ifdef LINUX
	if (instance_ == NULL){
		instance_ = std::unique_ptr<GaConfigurationManager>(new GaConfigurationManager);
	}
#else
	std::call_once(gOnceFlag,
		[] {
		instance_.reset(new GaConfigurationManager);
	});
#endif
	return *instance_.get();
}


GaConfigurationManager::GaConfigurationManager()
{
	
}
GaConfigurationManager::~GaConfigurationManager()
{
	//instance_.reset(nullptr);
}

void GaConfigurationManager::SetQRegisterSize(const size_t qRegisterSize)
{
	gQRegisterSize = qRegisterSize;

}
void GaConfigurationManager::SetGeneLength(const size_t geneLength)
{
	gGeneLength = geneLength;

}

void GaConfigurationManager::SetPopulationSize(const size_t populationSize)
{
	gPopulationSize = populationSize;

}

void GaConfigurationManager::SetIndividualMaxFitnessValue(const uint32_t maxValue)
{
	gIndividualMaxValue = maxValue;
}

size_t GaConfigurationManager::GetQRegisterSize() const
{
	return gQRegisterSize;
}
size_t GaConfigurationManager::GetGeneLength() const
{
	return gGeneLength;
}
size_t GaConfigurationManager::GetPopulationSize() const
{
	return gPopulationSize;
}

uint32_t GaConfigurationManager::GetIndividualMaxFitnessValue()const
{
	return gIndividualMaxValue;
}