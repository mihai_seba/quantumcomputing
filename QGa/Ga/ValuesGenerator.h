#pragma once
class ValuesGenerator
{
public:
	static ValuesGenerator &GetInstance();
	void GenerateGeneValue(uint8_t geneSize,const std::vector<int8_t>genome, int8_t *value);
	void GenerateUniformRealValue(uint8_t infLimit,uint8_t supLimit,double *value);
	void GenerateUniformIntegerValue(uint8_t infLimit, uint8_t supLimit, size_t *value);
	virtual ~ValuesGenerator();
protected:
	void GenerateRandomGeneValue(uint8_t geneSize, const std::vector<int8_t>genome, int8_t *value);
private:
	ValuesGenerator();
	ValuesGenerator(const ValuesGenerator& rs) = delete;
	ValuesGenerator& operator = (const ValuesGenerator& rs) = delete;
	static std::unique_ptr<ValuesGenerator> instance_;
	static std::once_flag gOnceFlag;
	
	std::mt19937 gEngine;
};