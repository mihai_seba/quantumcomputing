#pragma once
class  GaIndividual
{
	public:
		GaIndividual();
		virtual ~GaIndividual();
		void GenerateIndividual();
		uint32_t GetFitnessValue() const;
		void Initialize(const size_t geneLength,const size_t upperLimit);

		void SetFitnessValue(const uint32_t fitnessValue);
		int8_t GetGene(size_t idx) const;
		void SetGene(const size_t idx, const int8_t gene);
		size_t GetGeneMaxLength() const;
		std::string to_string() const;
		std::vector<int8_t> GetGenome() const;
		friend std::ostream& operator<<(std::ostream& os, const GaIndividual& obj)
		{
			os << obj.to_string();
			return os;
		}
	protected:
	private:	
		uint32_t gFitnessValue;
		size_t gGeneLength;
		size_t gUpperLimit;
		std::vector<int8_t> gGenes;
};


