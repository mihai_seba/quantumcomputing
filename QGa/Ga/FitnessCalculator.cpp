#include "stdafx.h"
#include "GaIndividual.h"
#include "GaConfigurationManager.h"
#include "FitnessCalculator.h"

FitnessCalculator::FitnessCalculator()
{

}

FitnessCalculator::~FitnessCalculator()
{

}


void FitnessCalculator::SetMaximumValue(uint32_t maxValue)
{
	gMaxValue = maxValue;
}



void FitnessCalculator::CalculateFitness(std::shared_ptr<GaIndividual> individual){
	
	
	//calculate the fitness of the current individual, respecting the notation:
	//negative value=0 on position "value"
	//positive value=1 on position "value"
	/*HOW TO
	*1. Decompose on bits maximum value for individual x ( calculated chromosome in RQGA and stored in fitness register)
	*2. For each matching bit between maximum value and individual gene, increment fitness value
	*/
	uint32_t fitness = 0;
	uint8_t position;
	uint8_t value;
	int8_t gene;
	uint8_t mask;
	size_t geneLength = GaConfigurationManager::GetInstance().GetGeneLength();
	for (size_t i = 0; i < geneLength; i++)
	{
		gene = individual->GetGene(i);
		position = std::abs(gene);
		if (gene < 0)
		{
			//value is 0
			value = 0;
		}
		else
		{
			value = 1;
		}
		mask = 1 << position;


		if ((gMaxValue&mask)!=0 && value==1){
			fitness++;
			
		}
		else{
			if (value == 0){
				fitness++;
			}
		}
	}
	(*individual).SetFitnessValue(fitness);
}

