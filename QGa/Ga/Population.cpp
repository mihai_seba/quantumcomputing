#include "stdafx.h"
#include "GaIndividual.h"
#include "GaConfigurationManager.h"
#include "FitnessCalculator.h"
#include "Population.h"


Population::Population()
{
	gPopulation = std::vector<std::shared_ptr<GaIndividual>>();
}


Population::~Population()
{
	gPopulation.clear();
}

void Population::SetPopulationSize(const size_t size){
	gPopulationSize = size;
	gPopulation.reserve(gPopulationSize);
	for (size_t i = 0; i < gPopulationSize; i++){
		gPopulation.push_back(nullptr);
	}
}
size_t Population::GetPopulationSize() const{
	return gPopulationSize;
}

void Population::Initialize(){
	std::shared_ptr<FitnessCalculator> fitnessCalculator = std::make_shared<FitnessCalculator>();
	GaConfigurationManager *configurationManager = &GaConfigurationManager::GetInstance();
	fitnessCalculator->SetMaximumValue(configurationManager->GetIndividualMaxFitnessValue());
		
	for (size_t i = 0; i < gPopulationSize; i++){
		std::shared_ptr<GaIndividual> individual = std::make_shared<GaIndividual>();
		individual->Initialize(configurationManager->GetGeneLength(), configurationManager->GetQRegisterSize());
		individual->GenerateIndividual();
		
		fitnessCalculator->CalculateFitness(individual);
		
		gPopulation[i]=individual;
	}
}

std::shared_ptr<GaIndividual> Population::GetIndividual(const size_t idx) const{
	return gPopulation.at(idx);
}

std::shared_ptr<GaIndividual> Population::GetFittestIndividual() const{
	std::shared_ptr<GaIndividual> fittestIndividual = gPopulation.at(0);
	
	for (std::shared_ptr<GaIndividual> ind : gPopulation){
		if (fittestIndividual->GetFitnessValue() < ind->GetFitnessValue()){
			fittestIndividual = ind;
		}
	}
	return fittestIndividual;
}

void Population::SaveIndividual(const int idx, const std::shared_ptr<GaIndividual> individual){
	gPopulation[idx] = individual;
}