#include "stdafx.h"
#include "ValuesGenerator.h"
#include "GaIndividual.h"


GaIndividual::GaIndividual(){
	
	
}

GaIndividual::~GaIndividual(){
	gGenes.clear();
}

void GaIndividual::Initialize(const size_t geneLength, const size_t upperLimit)
{
	gGeneLength = geneLength;
	gGenes.reserve(gGeneLength);
	gUpperLimit = upperLimit-1;
	for (size_t i = 0; i < gGeneLength; i++){
		gGenes.push_back(-1);
	}
}

size_t GaIndividual::GetGeneMaxLength() const
{
	return gUpperLimit;
}

void GaIndividual::GenerateIndividual(){
	ValuesGenerator *geneGenerator = &ValuesGenerator::GetInstance();
	for (size_t i = 0; i < gGeneLength; ++i){
		geneGenerator->GenerateGeneValue(gUpperLimit, gGenes, &gGenes[i]);
	}

}

std::string GaIndividual::to_string() const{
	std::stringstream strStream;
	for (int8_t gene : gGenes){
		//strStream << std::bitset<8>(gene).to_string()<<' ';
		strStream << std::to_string(gene) << ' ';
	}
	return strStream.str();
}

int8_t GaIndividual::GetGene(size_t idx) const {
	return gGenes.at(idx);
}

void GaIndividual::SetGene(const size_t idx, const int8_t gene){
	gGenes[idx] = gene;
}

uint32_t GaIndividual::GetFitnessValue() const{
	return gFitnessValue;
}

void GaIndividual::SetFitnessValue(const uint32_t fitnessValue){
	gFitnessValue = fitnessValue;
}


std::vector<int8_t> GaIndividual::GetGenome() const
{
	return gGenes;
}
