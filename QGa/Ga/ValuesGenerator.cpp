#include "stdafx.h"
#include <random>
#include "ValuesGenerator.h"


std::unique_ptr<ValuesGenerator>ValuesGenerator::instance_;
std::once_flag ValuesGenerator::gOnceFlag;

ValuesGenerator &ValuesGenerator::GetInstance()
{
#ifdef LINUX
	if (instance_ == NULL){
		instance_ = std::unique_ptr<ValuesGenerator>(new ValuesGenerator);
	}
#else
	std::call_once(gOnceFlag,
		[] {
		instance_.reset(new ValuesGenerator);
	});
#endif
	return *instance_.get();
}


ValuesGenerator::ValuesGenerator()
{
	std::random_device rd; // obtain a random number from hardware
	std::mt19937 gEngine(rd()); // seed the generator
}
ValuesGenerator::~ValuesGenerator()
{
	//instance_.reset(nullptr);
}

void ValuesGenerator::GenerateRandomGeneValue(uint8_t geneSize, const std::vector<int8_t>genome, int8_t *value)
{
	uint8_t position;
	uint8_t sign;
	bool isPositionAlreadyGenerated = true;

	//std::vector<uint8_t> generatedPositions;
	//std::copy(genome.begin(), genome.end(), generatedPositions.begin());

	/*Generate values between 0 and gUpperLimit*/
	std::uniform_int_distribution<> distrPosition(0, geneSize); // define the range
	std::uniform_int_distribution<> distrPositionSign(0, 1); // define the range

	isPositionAlreadyGenerated = true;
	while (isPositionAlreadyGenerated)
	{
		position = distrPosition(gEngine);
		if (std::find(genome.begin(), genome.end(), (-position)) == genome.end()
			&&
			std::find(genome.begin(), genome.end(), position) == genome.end())
		{
			isPositionAlreadyGenerated = false;
			break;
		}
	}

	sign = distrPositionSign(gEngine);
	if (sign == 1)
	{
		*value = position;//positive number
	}
	else
	{
		*value = (-1)*position;//negative number if sign is 0;
	}
}

void ValuesGenerator::GenerateGeneValue(uint8_t geneSize, const std::vector<int8_t>genome, int8_t *value)
{
	GenerateRandomGeneValue(geneSize, genome, value);
}



void ValuesGenerator::GenerateUniformRealValue(uint8_t infLimit, uint8_t supLimit, double *value)
{
	std::uniform_real_distribution<> distr(infLimit, supLimit);// define the range
	*value = distr(gEngine);
}

void ValuesGenerator::GenerateUniformIntegerValue(uint8_t infLimit, uint8_t supLimit, size_t *value)
{
	std::uniform_int_distribution<> distr(infLimit, supLimit); // define the range
	*value = distr(gEngine);
}