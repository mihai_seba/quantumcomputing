#pragma once
class EvolutionFactor
{
public:
	EvolutionFactor();
	virtual ~EvolutionFactor();
	void EvolvePopulation();
	void SetPopulation(std::shared_ptr<Population>population);
	std::shared_ptr<Population>GetNewGeneration();
protected:
private:
	
	std::shared_ptr<GaIndividual> CrossOver(const GaIndividual &x, const GaIndividual &y);
	void Mutate(std::shared_ptr<GaIndividual> individual);
	std::shared_ptr<GaIndividual> TournamentSelection();
	std::shared_ptr<Population>gPopulation;
	ValuesGenerator *gRandomValueGenerator;
	GaConfigurationManager *gConfigurationManager;
	const double UNIFORM_RATE = 0.7;
	const double TOLERANCE = 1e-10;
	const double MUTATION_RATE = 0.015;
	const size_t TOURNAMENT_SIZE = 5;
	const bool ELITISM = true;
	
};