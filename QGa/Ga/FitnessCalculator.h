#pragma once

class FitnessCalculator
{
public:
	FitnessCalculator();
	virtual ~FitnessCalculator();

	void SetMaximumValue(uint32_t maxValue);
	void CalculateFitness(std::shared_ptr<GaIndividual> individual);

private:

	uint32_t gMaxValue;
	
	
};

