#include "stdafx.h"
#include "ConfigurationUtils.h"
#include "QGaIndividual.h"



QGaIndividual::QGaIndividual()
{
	this->value = 0;
	this->capacity = 0;
	InitializeRegisters();
}


QGaIndividual::~QGaIndividual()
{
}

void QGaIndividual::InitializeRegisters()
{
	//get instance of ConfigurationUtils to access application constants;
	gConfigurationUtils = &ConfigurationUtils::GetInstance();
	//get instance of QRegisterOperations to access QRegister operations
	gRegisterOperations = &QRegisterOperations::GetInstance();
#pragma region Chrmosome register initialization
	//create chromosome quantum register
	gChromosomeRegister = std::shared_ptr<QRegister>(new QRegister);
	//reserve space for chromosome register
	gChromosomeRegister->reserve_space(gConfigurationUtils->CHROMOSOME_REGISTER_SIZE);
	//fill chromosome register with zeroes
	gRegisterOperations->FillWithZero(gChromosomeRegister.get());
#pragma endregion

#pragma region Fitness register initialization
	//create fitness quantum register
	gFitnessRegister = std::shared_ptr<QRegister>(new QRegister);
	//reserve space for fitness register
	gFitnessRegister->reserve_space(gConfigurationUtils->FITNESS_REGISTER_SIZE);
	//fill fitness register with zeroes
	gRegisterOperations->FillWithZero(gFitnessRegister.get());
#pragma endregion
}
bool QGaIndividual::IsValid()
{
	if (capacity == 0){
		CalculateValues();
	}
	return (capacity <= ConfigurationUtils::GetInstance().MAXIMUM_ALLOWED_PACKAGE_MASS);
}
QRegister &QGaIndividual::GetFitnessRegister()
{
	return *gFitnessRegister.get();
}
QRegister &QGaIndividual::GetChromosomeRegister()
{
	return *gChromosomeRegister.get();
}

void QGaIndividual::SetFitnessRegister(QRegister *qRegister)
{
	gFitnessRegister.reset(qRegister);
}
void QGaIndividual::SetChromosomeRegister(QRegister *qRegister)
{
	gChromosomeRegister.reset(qRegister);
}

void QGaIndividual::EntangleFitnessRegister()
{
	QRegister originalFitness = GetFitnessRegister();
	QRegister newQuantumRegister = QRegister(originalFitness.size() - 1);
	gRegisterOperations->CopyRegister(&newQuantumRegister, &originalFitness, 1);
}
Qubit QGaIndividual::GetEntangledFitnessRegister() const
{
	return gEntangledFitnessRegister;
}


void QGaIndividual::CalculateValues()
{

	for (size_t i = 0; i < gChromosomeRegister->size(); i++){
		if (gChromosomeRegister->at(i) == QubitOne()){
			switch (i)
			{
			case 0:
				// I1 (7 kg 40 $)
				capacity += 7;
				value += 4;
				break;
			case 1:
				// I2 (4 kg, 100 $)
				capacity += 4;
				value += 9;
				break;
			case 2:
				// I3 (2 kg, 50 $)
				capacity += 2;
				value += 5;
				break;
			case 3:
				// I4 (3 kg, 30)
				capacity += 3;
				value += 3;
				break;
			default:
				break;
			}
		}
	}
}

uint32_t QGaIndividual::GetValue() const {
	return value;
}
uint32_t QGaIndividual::GetCapacity() const {
	return capacity;
}

void QGaIndividual::SetFitnessValue(const double fitnessValue)
{
	gFitnessValue = fitnessValue;
}

double QGaIndividual::GetFitnessValue() const{
	return gFitnessValue;
}