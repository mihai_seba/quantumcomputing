#pragma once
class Oracle
{
public:
	Oracle();
	virtual ~Oracle();

	void ApplyOracle(Qubit *qubit, size_t registerSize);
	void SetPopulation(std::vector<QGaIndividual> population);
	void SetFitnessMaxValue(uint32_t fitnessMaxValue);
private:
	GateFactory gGateFactory;
	std::shared_ptr<IGate> gGateX;
	std::shared_ptr<IGate> gGateCNot;
	std::vector<QGaIndividual> gPopulationObj;
	uint32_t gFitnessMaxValue;
};

