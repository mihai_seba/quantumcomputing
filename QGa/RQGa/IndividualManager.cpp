#include "stdafx.h"
#include "ConfigurationUtils.h"
#include "QGaIndividual.h"
#include "IndividualManager.h"


std::unique_ptr<IndividualManager>IndividualManager::instance_;
std::once_flag IndividualManager::gOnceFlag;


IndividualManager &IndividualManager::GetInstance()
{
#ifdef LINUX
	if(instance_==NULL){
		instance_=std::unique_ptr<IndividualManager>(new IndividualManager());
	}
#else
	std::call_once(gOnceFlag,
		[] {
		instance_.reset(new IndividualManager);
	});
#endif
	return *instance_.get();
}

void IndividualManager::ChangeChromosomeRegister(QGaIndividual *individual, const  std::string pattern)
{
	//get chromosome register from individual
	//QRegister *chromosomeRegister = &individual->GetChromosomeRegister();
	////refill chromosome register qbit by qbit
	for (size_t i = 0; i < individual->GetChromosomeRegister().size(); i++)
	{
		if (pattern.at(i) == '1'){
			individual->GetChromosomeRegister()[i] = QubitOne();
		}
		else{
			if (pattern.at(i) == '0'){
				individual->GetChromosomeRegister()[i] = QubitZero();
			}


		}
	}
}

void IndividualManager::MarkFixedStatesInFitnessRegister(QGaIndividual *individual, std::vector<int8_t> states)
{
	//negative value=0 on position "value"
	//positive value=1 on position "value"
	size_t qRegisterSize = individual->GetFitnessRegister().size()-1;//Escape valid individual qubit
	int8_t maximValue = *std::max_element(states.begin(), states.end());
	assert(std::abs(maximValue) <= qRegisterSize);
	for (int8_t state : states)
	{
		if(std::abs(state)==0)
		{
			//Continue if state is 0 ( position 0);
			continue;
		}
		if (state < 0)
		{
			
			individual->GetFitnessRegister()[std::abs(state)] = QubitZero();
			
		}
		else
		{
			
			individual->GetFitnessRegister()[std::abs(state)] = QubitOne();
			
		}
		individual->GetFitnessRegister()[std::abs(state)].MeasureQubit();
	}
}

void IndividualManager::ChangeFitnessRegister(QGaIndividual *individual, const uint32_t fitnessValue)
{
#define BITSIZE 9 //this macro represent the number of bits for representing the fitness value;
	//since bitset accepts only constant values, it couldn't be used configuration->REGISTER_SIZE-1
	std::string fitnessString = std::bitset<BITSIZE>(fitnessValue).to_string();
	std::string fitnessFinalString = "";
	//if individual is valid, first bit of the register will be 1, otherwise 0
	if (individual->IsValid()){
		fitnessFinalString = "1" + fitnessString;
	}
	else{
		fitnessFinalString = "0" + fitnessString;
	}
	//refill fitness register qbit by qbit
	for (size_t i = 0; i < individual->GetFitnessRegister().size(); i++){
		if (fitnessFinalString.at(i) == '1'){
			individual->GetFitnessRegister()[i] = QubitOne();
		}
		else{
			if (fitnessFinalString.at(i) == '0'){
				individual->GetFitnessRegister()[i] = QubitZero();
			}
		}
	}

}


void IndividualManager::CalculateFitnessValue(const QGaIndividual &individual, uint32_t *fitnessValue){
	//get ConfigurationUtils instance
	ConfigurationUtils *configuration = &ConfigurationUtils::GetInstance();
	double fitnessFloatValue = individual.GetValue() - (configuration->TOTAL_ADDED_VALUE + 1)*(individual.GetCapacity() / configuration->MAXIMUM_ALLOWED_PACKAGE_MASS);
	*fitnessValue = std::abs(fitnessFloatValue);
}


