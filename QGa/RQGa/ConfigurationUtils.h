#pragma once
class ConfigurationUtils
{
public:
#pragma region Singleton specific methods
	static ConfigurationUtils &GetInstance();
	inline virtual ~ConfigurationUtils(){}
#pragma endregion
#pragma region Constant fields
	const size_t CHROMOSOME_REGISTER_SIZE = 4;//2
	const size_t FITNESS_REGISTER_SIZE = 10;//3
	static const std::vector<std::string> FILL_PATTERNS;
	static const size_t NUMBER_OF_INDIVIDUALS;
	const size_t GENE_LENGTH = 2;
#pragma endregion
#pragma region Problem specific constant fields
	const size_t TOTAL_ADDED_VALUE = 22;//6;
	const double MAXIMUM_ALLOWED_PACKAGE_MASS = 10;
#pragma endregion
protected:

private:
#pragma region Singleton fields
	static std::unique_ptr<ConfigurationUtils> instance_;
	static std::once_flag gOnceFlag;
	inline ConfigurationUtils(){}
	ConfigurationUtils(const ConfigurationUtils& rs) = delete;
	ConfigurationUtils& operator = (const ConfigurationUtils& rs) = delete;
#pragma endregion

};

