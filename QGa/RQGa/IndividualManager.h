#pragma once
class IndividualManager
{
public:
#pragma region Singleton specific methods
	static IndividualManager &GetInstance();
	inline virtual ~IndividualManager(){}
#pragma endregion
	void ChangeChromosomeRegister(QGaIndividual *individual, const std::string pattern);
	void ChangeFitnessRegister(QGaIndividual *individual, const  uint32_t fitnessValue);
	void CalculateFitnessValue(const QGaIndividual &individual, uint32_t *fitnessValue);
	void MarkFixedStatesInFitnessRegister(QGaIndividual *individual, const std::vector<int8_t> states);
protected:

private:
	
#pragma region Singleton fields
	static std::unique_ptr<IndividualManager> instance_;
	static std::once_flag gOnceFlag;
	inline IndividualManager(){	}
	IndividualManager(const IndividualManager& rs) = delete;
	IndividualManager& operator = (const IndividualManager& rs) = delete;
#pragma endregion
};

