#include "stdafx.h"
#include "QCpp.h"
#include "ConfigurationUtils.h"
#include "QGaIndividual.h"
#include "Oracle.h"


Oracle::Oracle()
{
	gGateFactory = GateFactory();
	gGateX = gGateFactory.CreateGate(EXGate);
	gGateCNot = gGateFactory.CreateGate(ECnotGate);
}


Oracle::~Oracle()
{
}


void Oracle::ApplyOracle(Qubit *qubit, size_t registerSize)
{
	std::vector<size_t>affectedPosition;
	for (size_t i = 0; i < gPopulationObj.size();i++){
		if (gPopulationObj.at(i).IsValid() && gPopulationObj.at(i).GetFitnessValue()>gFitnessMaxValue){
			//mark position for applying X-Gate
			affectedPosition.push_back(i);
		}
	}
	//apply oracle 
	gGateX->Apply(qubit, affectedPosition, registerSize);
	gGateCNot->Apply(qubit, ConfigurationUtils::NUMBER_OF_INDIVIDUALS, affectedPosition, registerSize);
	gGateX->Apply(qubit, affectedPosition, registerSize);
}

void Oracle::SetPopulation(std::vector<QGaIndividual> population)
{
	gPopulationObj = population;
}

void Oracle::SetFitnessMaxValue(uint32_t fitnessMaxValue)
{
	gFitnessMaxValue = fitnessMaxValue;
}
