#include "stdafx.h"
#include "ConfigurationUtils.h"
#include "QGaIndividual.h"
#include "IndividualManager.h"
#include "QCpp.h"
#include "Oracle.h"
#include "GroverImplementation.h"
#include "Ga.h"
#include "RQGaAlgorithm.h"


RQGaAlgorithm::RQGaAlgorithm()
{
	gConfiguration = &ConfigurationUtils::GetInstance();
	gIndividualManager = &IndividualManager::GetInstance();
	gRegisterOperations = &QRegisterOperations::GetInstance();
	gGroverImplementation = std::make_shared<GroverImplementation>();
	gOracleObj = std::make_shared<Oracle>();
	gPopulation.reserve(ConfigurationUtils::NUMBER_OF_INDIVIDUALS);
#pragma region Classic GA initialization
	gGa = std::make_shared<GeneticAlgorithm>();
	GaConfigurationManager *gaConfiguration = &GaConfigurationManager::GetInstance();
	gaConfiguration->SetQRegisterSize(gConfiguration->FITNESS_REGISTER_SIZE);
	gaConfiguration->SetPopulationSize(ConfigurationUtils::NUMBER_OF_INDIVIDUALS);
	gaConfiguration->SetGeneLength(gConfiguration->GENE_LENGTH);
#pragma endregion
}


RQGaAlgorithm::~RQGaAlgorithm()
{
}

void RQGaAlgorithm::InitializePopulation()
{
	uint32_t fitnessValue = 0;
	std::vector<int8_t> markedStates;
	for (size_t i = 0; i < ConfigurationUtils::NUMBER_OF_INDIVIDUALS; i++){
		QGaIndividual individual = QGaIndividual();
		gIndividualManager->ChangeChromosomeRegister(&individual, ConfigurationUtils::FILL_PATTERNS.at(i));
		individual.CalculateValues();
		gIndividualManager->CalculateFitnessValue(individual, &fitnessValue);
		//initialize Classic GA with fitness value;
		GaConfigurationManager::GetInstance().SetIndividualMaxFitnessValue(fitnessValue);
		gGa->RunAlgorithm();
		markedStates=gGa->GetGenome();
		gIndividualManager->ChangeFitnessRegister(&individual, fitnessValue);
		gIndividualManager->MarkFixedStatesInFitnessRegister(&individual, markedStates);
		individual.SetFitnessValue(fitnessValue);
		individual.EntangleFitnessRegister();
		gPopulation.push_back(individual);
	}
	
	

}

void RQGaAlgorithm::RunAlgorithm()
{
	Qubit resultQubit;
	size_t registerMaxValueIndex = 0;
	size_t fitnessRegisterSize = ConfigurationUtils::GetInstance().FITNESS_REGISTER_SIZE;//WAS +1 but why?
	uint32_t maxValueLowerBound = std::pow(2, (fitnessRegisterSize+1));
	uint32_t maxValueUpperBound = std::pow(2, (fitnessRegisterSize + 2)) - 1.0;
	std::random_device rd; // obtain a random number from hardware
	std::mt19937 eng(rd()); // seed the generator


	//set population in oracle
	gOracleObj->SetPopulation(gPopulation);

	
	//should be between 2^(M+1)<=max<=2^(M+2)-1, M is the number of qubits in fitness register
	std::uniform_int_distribution<> distr(maxValueLowerBound, maxValueUpperBound); // define the range
	
	gMaxValue = distr(eng);
	gOracleObj->SetFitnessMaxValue(gMaxValue);


	
	

	std::vector<Qubit> searchSpace;
	for (QGaIndividual i : gPopulation){
		i.EntangleFitnessRegister();
		searchSpace.push_back(i.GetEntangledFitnessRegister());
	}
	gGroverImplementation->SetOracle(gOracleObj);	
	
	for (size_t i = 0; i < ConfigurationUtils::NUMBER_OF_INDIVIDUALS; i++){
		gGroverImplementation->SetSearchSpace(searchSpace);
		gGroverImplementation->Init();
		gGroverImplementation->Run();
		gGroverImplementation->Measure();
		gGroverImplementation->ExtractResult(&registerMaxValueIndex);
		gMaxValue = gPopulation.at(registerMaxValueIndex).GetFitnessValue();
		gOracleObj->SetFitnessMaxValue(gMaxValue);

	}
	std::cout << '\n';

}



void RQGaAlgorithm::PrintResult()
{
	std::cout << "\nSolution:" << gMaxValue << '\n';
}
