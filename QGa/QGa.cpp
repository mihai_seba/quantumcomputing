// QGa.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Ga.h"
#include "RQGa.h"



int main(int argc, char* argv[])
{
	
	auto t_start = std::chrono::high_resolution_clock::now();
	std::unique_ptr<RQGaAlgorithm> rqgaAlgorithm(new RQGaAlgorithm);
	rqgaAlgorithm->InitializePopulation();
	rqgaAlgorithm->RunAlgorithm();
	rqgaAlgorithm->PrintResult();

	auto t_end = std::chrono::high_resolution_clock::now();
	std::cout << std::chrono::duration<double, std::milli>(t_end - t_start).count()
		<< " ms\n";
	rqgaAlgorithm.reset();
	return 0;
}






