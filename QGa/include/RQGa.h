#pragma once
#include "../RQGa/ConfigurationUtils.h"
#include "../RQGa/QGaIndividual.h"
#include "../RQGa/IndividualManager.h"
#include "../RQGa/GroverImplementation.h"
#include "../RQGa/RQGaAlgorithm.h"