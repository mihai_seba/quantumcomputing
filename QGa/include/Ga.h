#pragma once
#include "../Ga/GaIndividual.h"
#include "../Ga/FitnessCalculator.h"
#include "../Ga/Population.h"
#include "../Ga/ValuesGenerator.h"
#include "../Ga/GaConfigurationManager.h"
#include "../Ga/EvolutionFactor.h"
#include "../Ga/GeneticAlgorithm.h"