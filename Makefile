CC=g++

all: install_lib grover rqga ga qga


install_lib:
	cp Libs/LibQpp/Linux/so/libqpp.so.1.0 /usr/lib
	ln -sf /usr/lib/libqpp.so.1.0 /usr/lib/libqpp.so.1
	ln -sf /usr/lib/libqpp.so.1.0 /usr/lib/libqpp.so
	cp Libs/CpuTimeProfilerDll/Linux/so/libcputimeprofiler.so.1.0 /usr/lib
	ln -sf /usr/lib/libcputimeprofiler.so.1.0 /usr/lib/libcputimeprofiler.so.1
	ln -sf /usr/lib/libcputimeprofiler.so.1.0 /usr/lib/libcputimeprofiler.so
grover:
	cd GroverAlgo && make
rqga:
	cd RQGa && make
	
ga:
	cd GeneticAlgorithmApp && make

qga:
	cd QGa && make
	
clean:
	cd RQGa && make clean
	cd GroverAlgo && make clean
	cd GeneticAlgorithmApp && make clean
	cd QGa && make clean